package ir.aut.GameEngine;

import ir.aut.GameEngine.Components.Transform;
import ir.aut.GameEngine.Core.Camera;
import ir.aut.GameEngine.Core.GameEvents;
import ir.aut.GameEngine.Core.GameObject;
import ir.aut.GameEngine.Input.Mouse;
import ir.aut.GameEngine.Vector.Vector2D;
import ir.aut.TanksGame.SoftWall;
import ir.aut.TanksGame.Tank;
import ir.aut.TanksGame.TankGunMissle;

import java.awt.*;
import java.util.ArrayList;

/**
 * <h1>Game's Level</h1>
 * A singleton class that represents whole of game level (scene).
 *
 * <h2>What should we do here ?</h2>
 * Just add any game objects to {@code gameObjects} variable within Scene's constructor and <strong>setting sound listener</strong>.<br>
 * Note that the adding order is relevant ( So , {@code start} , {@code update} and {@code onGUI} of later-added game objects are called after early-added game objects. )<br>
 * <font color="red">Warning : Don't modify anything other than the constructor's body.</font>
 *
 *
 * @author MMKH
 *
 */
public final class Scene implements GameEvents {

    private static Scene instance = null;
    private ArrayList<GameObject> gameObjects = new ArrayList<>();

    private GameObject soundListener;
    private boolean gameOver = false;
    private ArrayList<GameObject> pendingRemove  = new ArrayList<>();
    private ArrayList<GameObject> pendingAdd  = new ArrayList<>();

    /**
     * Holds height of screen (which is set from {@code GameFrame} class).
     */
    public static int GAME_HEIGHT ;

    /**
     * Holds width of screen (which is set from {@code GameFrame} class).
     */
    public static int GAME_WIDTH ;

    /**
     * Is on debugging mode ? (Enables some features like printing realtime FPS or drawing collider bounds and etc.).
     */
    public static final boolean debuggingMode = false;

    public static long deltaTime;

    public static int renderingObjectsCount;

    /**
     * Gets the singleton instance of Scene.
     * @return
     */
    public static Scene getInstance(){
        if(instance==null)
            instance = new Scene();
        return instance;
    }


    /**
     * Place for adding game objects to scene .<br>
     * <strong>For example :</strong><br>
     * {@code gameObjects.add(new Tank(...));}<br><br>
     *
     * <font color="blue">Note : Always , add {@code Camera.getInstance()} as the first game object.</font><br>
     * <font color="red">Warning : Don't forget to set sound listener game object (Which usually is the player's game object).</font>
     * <br>
     *
     */
    private Scene() {

        // Add Camera
        gameObjects.add(Camera.getInstance());

        // Add GameObjects
        new MapLoader("src/ir/aut/TanksGame/Resources/Maps/1.txt").createScene(gameObjects);

//        gameObjects.add();
        //gameObjects.add(new Tank("TankGun",new Transform(new Vector2D(0,0),Math.toRadians(0),new Vector2D(100,100),new Vector2D(0,0))));
        //gameObjects.get(2).getCollider().setTrigger(true);
//        gameObjects.get(1).addChildGameObject(gameObjects.get(2));
//        gameObjects.add(new Tank("Player2",new Transform(new Vector2D(600,100),Math.toRadians(0),new Vector2D(200,100),new Vector2D(0,0))));
//        gameObjects.add(new Tank("Object",new Transform(new Vector2D(400,300),0,new Vector2D(100,100),new Vector2D(0,0))));
//        gameObjects.add(new SoftWall("SoftWall",new Transform(new Vector2D(400,300),0,new Vector2D(100,100),new Vector2D(0,0))));
//        gameObjects.add(new SoftWall("SoftWall",new Transform(new Vector2D(500,300),0,new Vector2D(100,100),new Vector2D(0,0))));
//        gameObjects.add(new SoftWall("SoftWall",new Transform(new Vector2D(600,300),0,new Vector2D(100,100),new Vector2D(0,0))));
        gameObjects.add(new Tank("Player",new Transform(new Vector2D(200,200),Math.toRadians(0),new Vector2D(100,100),new Vector2D(0,0))));

//        Camera.getInstance().addChildGameObject(gameObjects.get(gameObjects.size()-1));


        // Set sound listener
        setSoundListener(gameObjects.get(gameObjects.size()-1));
    }

    /**
     * Calls {@code start} method for every added game object (just once , at beginning of game).
     */
    @Override
    public void start() {
        for (GameObject gameObject :
                gameObjects) {
            gameObject.start();
        }
        performAdds();
        performRemoves();
    }

    /**
     * Calls {@code update} method for every added game object (every frame).
     */
    @Override
    public void update() {
        for (GameObject gameObject :
                gameObjects) {
            gameObject.update();
        }
        performAdds();
        performRemoves();
    }

    /**
     * Calls {@code onGUI} method for every added game object (every frame).<br>
     * This is used just for drawing on screen.
     *
     * @param g Graphics2D object for drawing on screen.
     */
    @Override
    public void onGUI(Graphics2D g) {
        renderingObjectsCount=0;
        for (GameObject gameObject :
                gameObjects) {
            gameObject.onGUI(g);
        }
        if(debuggingMode) {
            g.setColor(Color.BLACK);
            g.drawString(Mouse.getInstance().getMouseX() + " , " + Mouse.getInstance().getMouseY(), 100, 100);
            g.drawString("Rendering objects count : " + renderingObjectsCount, 100, 200);
        }
        performAdds();
        performRemoves();
    }

    /**
     * Removes any game object which was destroyed in last frame update.
     */
    private void performRemoves(){
        for(GameObject gameObject : pendingRemove){
            gameObjects.remove(gameObject);
        }
        pendingRemove.clear();
    }

    /**
     * Adds any game object which was added in last frame update.
     */
    private void performAdds(){
        ArrayList<GameObject> beingAdded = new ArrayList<>();
        beingAdded.addAll(pendingAdd);
        pendingAdd.clear();
        for(GameObject gameObject : beingAdded){
            gameObjects.add(gameObject);
            gameObject.start();
        }

    }

    /**
     * Deactivates the gameObject and adds it to removing queue to being removed after the current frame is processed.
     * @param gameObject
     */
    public void destroyGameObject(GameObject gameObject){
        gameObject.setActive(false);
        pendingRemove.add(gameObject);
    }

    /**
     * Adds a new game object to Scene.
     * @param gameObject
     */
    public void instantiate(GameObject gameObject){
        pendingAdd.add(gameObject);
    }

    /**
     * Gets any gameObjects of game's scene.
     * @return
     */
    public ArrayList<GameObject> getGameObjects() {
        return gameObjects;
    }

    /**
     * Searches for game objects with the given id.
     * @param id
     * @return ArrayList of founded game objects.
     */
    public ArrayList<GameObject> findGameObjectsById(String id){
        ArrayList<GameObject> foundGameObjects = new ArrayList<>();
        for (GameObject gameObject :
                gameObjects) {
            if (gameObject.getId().equals(id))
                foundGameObjects.add(gameObject);
        }

        return foundGameObjects;
    }

    /**
     * Is game over ?
     * @return
     */
    public boolean isGameOver() {
        return gameOver;
    }

    /**
     * Sets whether is gameOver or not.
     * @param gameOver
     */
    public void setGameOver(boolean gameOver) {
        this.gameOver = gameOver;
    }

    /**
     * Gets sound listener game object of scene (Which usually is the player's game object).
     * @return
     */
    public GameObject getSoundListener() {
        return soundListener;
    }

    /**
     * Sets sound listener game object of scene (Which usually is the player's game object).
     * @param soundListener GameObject of sound listener
     */
    public void setSoundListener(GameObject soundListener) {
        this.soundListener = soundListener;
    }
}
