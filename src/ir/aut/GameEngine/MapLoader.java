package ir.aut.GameEngine;

import ir.aut.GameEngine.Components.Transform;
import ir.aut.GameEngine.Core.GameObject;
import ir.aut.GameEngine.Vector.Vector2D;
import ir.aut.TanksGame.*;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;

public class MapLoader {

    private HashMap<Character,Class<? extends GameObject>> charClass = new HashMap<>() ;
    private String mapFileAddress;

    public MapLoader(String mapFileAddress){
        initCharClassMap();
        this.mapFileAddress = mapFileAddress;
    }

    public void createScene(ArrayList<GameObject> gameObjects){
        for(int i=0 ; i<50 ; i++)
            for(int j=0 ; j<25 ; j++)
                gameObjects.add(new Ground("GND",new Transform(new Vector2D(j*100 + 50 , i*100 + 50),0,new Vector2D(100,100),Vector2D.zero())));
        File file = new File(mapFileAddress);
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
            String st;
            int i = 0;
            while ((st = br.readLine()) != null){
                char[] line = st.toCharArray();
                for (int j = 0; j < line.length; j++) {
                    if(line[j] != ' ')
                    gameObjects.add((charClass.get(line[j])).getConstructor(String.class,Transform.class).newInstance(String.valueOf(line[j]),new Transform(new Vector2D(j*100 + 50,i*100 + 50) , 0 , new Vector2D(100,100) , Vector2D.zero())));
                }
                i++;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } finally {
            if(br!=null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


    }

    private void initCharClassMap(){
        charClass.put('P',Tank.class); // Player Tank
        charClass.put('#',HardWall.class); // HardWall
        charClass.put('$',SoftWall.class); // SoftWall
        charClass.put('T',TeazelWall.class); // Teazel
        charClass.put('V',Plant.class); // Plant

    }

}
