package ir.aut.GameEngine.Core;

import java.awt.*;

/**
 * <h1>Standard Game Events</h1>
 * Standard and globally used game events.
 *
 * <h2>What should we do here ?</h2>
 * Nothing , that's done . {@code Scene} , {@code GameObject} and {@code Component} classes are implementing this interface .
 *
 * @author MMKH
 *
 */
public interface GameEvents {

    /**
     * Runs only first time the {@code Scene} is loaded.
     */
    void start();

    /**
     * Runs every frame.
     */
    void update();

    /**
     * Runs every frame , but for GUI commands (which uses Graphics2D object).
     * @param g Graphics2D object for drawing on screen.
     */
    void onGUI(Graphics2D g);

}
