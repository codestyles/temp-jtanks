package ir.aut.GameEngine.Core;

import ir.aut.GameEngine.Components.Transform;
import ir.aut.GameEngine.Vector.Vector2D;

/**
 * <h1>Camera Viewport</h1>
 * A singleton class that is a {@code GameObject} , and has {@code Transform} component. It's {@code Location} is upper-left corner of viewport and other features of {@code Transform} is not used here.
 *
 * <h2>What should we do here ?</h2>
 * Nothing here , but you should add this singleton object as first {@code GameObject} of {@code Scene}.
 *
 * @author MMKH
 *
 */
public final class Camera extends GameObject {

    private static Camera instance = null;

    /**
     * Gets the singleton instance of Camera.
     * @return
     */
    public static Camera getInstance(){
        if(instance==null)
            instance = new Camera("CAMERA",new Transform(new Vector2D(0,0),0,new Vector2D(0,0),new Vector2D(0,0)));
        return instance;
    }

    private Camera(String id, Transform transform) {
        super(id, transform);
    }



}
