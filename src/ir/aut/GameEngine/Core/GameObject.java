package ir.aut.GameEngine.Core;

import ir.aut.GameEngine.Components.Animation.Animator;
import ir.aut.GameEngine.Components.Audio.SoundPlayer;
import ir.aut.GameEngine.Components.Collider.BoxCollider;
import ir.aut.GameEngine.Components.Component;
import ir.aut.GameEngine.Components.Rendering.SpriteRenderer;
import ir.aut.GameEngine.Components.Transform;
import ir.aut.GameEngine.Scene;

import java.awt.*;
import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * <h1>GameObject (Without inheritance , it's equivalent to unity's {@code EmptyObject})</h1>
 * A class that represents a game object .<br>
 * <br>
 * <strong>Default components of a gameObject :</strong><br>
 * {@code Transform} (*)<br>
 * {@code SpriteRenderer}<br>
 * {@code BoxCollider}<br>
 * {@code Animator}<br>
 * {@code SoundPlayer}<br>
 * <font color="blue">(*) Note : Only {@code Transform} component is created by default. others could be created and assigned in constructor of classes which inherits {@code GameObject}</font><br>
 *
 * <h2>What should we do here ?</h2>
 * Nothing here , but for adding a new GameObject type to game , follow these instructions :<br>
 * <p>
 *     1. Create a class , which extends {@code GameObject} class.<br>
 *     2. In it's constructor method , after calling {@code super(...)} , create required default components (except {@code Transform} which is created by default) and assign them to their pointer variables of {@code GameObject} class.<br>
 *     3. Create required custom components and add them with {@code components.add(...)}.<br>
 *     Finished ! Now , don't forget to create and add real objects from you GameObject extended class in constructor of {@code Scene} class.<br>
 * </p>
 *
 * @author MMKH
 *
 */
public class GameObject implements GameEvents {

    private String id;
    private boolean isActive = true;
    private GameObject parent;
    private Transform transform;
    protected SpriteRenderer renderer;
    protected BoxCollider collider;
    protected Animator animator;
    protected SoundPlayer soundPlayer;
    protected ArrayList<Component> components = new ArrayList<>();
    protected ArrayList<GameObject> childs = new ArrayList<>();

    /**
     * Creates a new GameObject with given id and transform.
     * @param id
     * @param transform
     */
    public GameObject(String id,Transform transform) {
        this.id = id;
        this.parent = null;
        this.transform = transform;
    }


    /**
     * Calls {@code start} method for every default owned or added components (just once , at beginning of game).
     */
    @Override
    public final void start() {
        registerComponent(this.transform);
        transform.start();
        if(renderer!=null){
            registerComponent(this.renderer);
            renderer.start();
        }
        if(collider!=null){
            registerComponent(this.collider);
            collider.start();
        }
        if(animator!=null){
            registerComponent(this.animator);
            animator.start();
        }
        if(soundPlayer!=null){
            registerComponent(this.soundPlayer);
            soundPlayer.start();
        }
        for (Component component :
                components) {
            registerComponent(component);
            component.start();
        }
    }

    /**
     * Calls {@code update} method for every default owned or added components (every frame).
     */
    @Override
    public final void update() {
        if(isActive) {
            if(transform.isActive())
                transform.update();
            if(renderer!=null && renderer.isActive()) renderer.update();
            if(collider!=null && collider.isActive()) collider.update();
            if(animator!=null && animator.isActive()) animator.update();
            if(soundPlayer!=null && soundPlayer.isActive()) soundPlayer.update();
            for (Component component :
                    components) {
                if(component.isActive())
                    component.update();
            }
        }
    }

    /**
     * Calls {@code onGUI} method for every default owned or added components (every frame).<br>
     * This is used just for drawing on screen.
     *
     * @param g Graphics2D object for drawing on screen.
     */
    @Override
    public final void onGUI(Graphics2D g) {
        if(isActive) {
            if(transform.isActive())
                transform.onGUI(g);
            if(renderer!=null && renderer.isActive()) renderer.onGUI(g);
            if(collider!=null && collider.isActive()) collider.onGUI(g);
            if(animator!=null && animator.isActive()) animator.onGUI(g);
            if(soundPlayer!=null && soundPlayer.isActive()) soundPlayer.onGUI(g);
            for (Component component :
                    components) {
                if(component.isActive())
                    component.onGUI(g);
            }
        }
    }

    /**
     * Make's the given game object to be a child of current one.
     * @param gameObject child GameObject
     */
    public void addChildGameObject(GameObject gameObject){
        gameObject.setParent(this);
        childs.add(gameObject);
    }

    /**
     * Destroys this game object.
     */
    public void destroy(){
        for(GameObject gameObject : childs)
            Scene.getInstance().destroyGameObject(gameObject);
        Scene.getInstance().destroyGameObject(this);
    }

    public String getId() {
        return id;
    }

    public boolean isActive() {
        return isActive;
    }

    public GameObject getParent() {
        return parent;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    private void setParent(GameObject parent) {
        this.parent = parent;
    }

    public Transform getTransform() {
        return transform;
    }

    public SpriteRenderer getRenderer() {
        return renderer;
    }

    public BoxCollider getCollider() {
        return collider;
    }

    public Animator getAnimator() {
        return animator;
    }

    public SoundPlayer getSoundPlayer() {
        return soundPlayer;
    }

    public ArrayList<Component> getComponents() {
        return components;
    }


    /**
     * Returns an arrayList of components of this gameObject with given type.
     * @param componentType Component type class name.
     * @param <T> Component type.
     * @return ArrayList of given component type.
     */
    public <T extends Component> ArrayList<T> getComponents(Class<T> componentType) {
        ArrayList<T> foundedComponents = new ArrayList<>();
        for(Component component : components)
            if(component.getClass().equals(componentType) ) {
                foundedComponents.add((T)component);
            }
        return  foundedComponents;
    }

    public void registerComponent(Component component){
        component.setGameObject(this);
    }
}
