package ir.aut.GameEngine.Components;

import ir.aut.GameEngine.Core.Camera;
import ir.aut.GameEngine.Vector.Vector2D;

import java.awt.*;

/**
 * <h1>Coordinates and physical characteristics holder</h1>
 * A class that represents a Transform component , which has a location , rotation , scale and pivot.<br>
 *
 * <h2>What should we do here ?</h2>
 * Nothing . This component is added to any game object by default.
 *
 * @author MMKH
 *
 */
public final class Transform extends Component {

    /**
     * Location of transform . Measured from (0,0) (which is upper-left corner of the screen at game start) to pivot location in the screen.
     */
    private Vector2D location;

    /**
     * Rotation angle of transform . Measured in standard math polar coordinates system.
     */
    private double rotation;

    /**
     * Scale of transform in pixels (Not ratio).
     */
    private Vector2D scale;

    /**
     * Location offset of pivot point from center point of transform.
     */
    private Vector2D pivot;

    /**
     * Not used.
     */
    @Override
    public void start() {

    }

    /**
     * Not used.
     */
    @Override
    public void update() {

    }

    /**
     * Not used.
     * @param g Graphics2D object for drawing on screen.
     */
    @Override
    public void onGUI(Graphics2D g) {

    }

    /**
     * Creates a new Transform component with given location , rotation , scale and pivot.
     * @param location Location of transform . Measured from (0,0) (which is upper-left corner of the screen at game start) to pivot location in the screen.
     * @param rotation Rotation angle of transform . Measured in standard math polar coordinates system.
     * @param scale Scale of transform in pixels (Not ratio).
     * @param pivot Location offset of pivot point from center point of transform.
     */
    public Transform(Vector2D location, double rotation, Vector2D scale, Vector2D pivot) {
        this.location = location;
        this.rotation = rotation;
        this.scale = scale;
        this.pivot = pivot;
    }

    /**
     * Creates a new Transform component from given transform (In fact , it clone's the given transform).
     * @param t
     */
    public Transform(Transform t){
        this(t.location,t.rotation,t.scale,t.pivot);
        setGameObject(t.getGameObject());
    }

    /**
     * Moves this transform by given location offset , without applying collisions.<br>
     * Compare with {@code BoxCollider.move(...)}
     * @param locationOffset
     */
    public void move(Vector2D locationOffset){
        if(!locationOffset.isZeroVector())
        this.location = Vector2D.add(this.location,locationOffset);
    }

    /**
     * Rotates this transform by given rotation offset , without applying collisions.<br>
     * Compare with {@code BoxCollider.rotate(...)}
     * @param rotationOffset (In radians)
     */
    public void rotate(double rotationOffset){
        if(rotationOffset!=0) {
            if (this.rotation + rotationOffset > Math.toRadians(360))
                this.rotation += rotationOffset - Math.toRadians(360);
            else if (this.rotation + rotationOffset < 0)
                this.rotation += rotationOffset + Math.toRadians(360);
            else
                this.rotation += rotationOffset;
        }
    }

    /**
     * Scales this transform by given scale offset , without applying collisions.<br>
     * Compare with {@code BoxCollider.scale(...)}
     * @param scaleOffset (In pixels, not ratio)
     */
    public void scale(Vector2D scaleOffset){
        if(!scaleOffset.isZeroVector())
        this.scale = Vector2D.add(this.scale,scaleOffset);
    }

    /**
     * Returns the location of transform . Measured from (0,0) (which is upper-left corner of the screen at game start) to pivot location in the screen.
     * @return
     */
    public Vector2D getLocation() {
        return Vector2D.add(location, (getGameObject().getParent() != null ? getGameObject().getParent().getTransform().getLocation() : Vector2D.zero()));
    }

    /**
     * Returns the location of transform . Measured from (0,0) (which is upper-left corner of the screen at game start) to pivot location in the screen.
     * @return
     */
    public Vector2D getLocation(boolean onScreenLocation) {
        if(!onScreenLocation) return getLocation();
//        return Vector2D.add(Vector2D.add(location, (getGameObject().getParent() != null ? getGameObject().getParent().getTransform().getLocation() : Vector2D.zero())),(getGameObject().getParent() == null && !this.equals(Camera.getInstance().getTransform()) ? Camera.getInstance().getTransform().getLocation().negative() : Vector2D.zero()));
        return Vector2D.add(Vector2D.add(location, (getGameObject().getParent() != null ? getGameObject().getParent().getTransform().getLocation() : Vector2D.zero())),(!this.equals(Camera.getInstance().getTransform()) ? Camera.getInstance().getTransform().getLocation().negative() : Vector2D.zero()));
    }

    public Vector2D getLocalLocation(){
        return Vector2D.add(location,(getGameObject().getParent() == null && !this.equals(Camera.getInstance().getTransform()) ? Camera.getInstance().getTransform().getLocation().negative() : Vector2D.zero()));
    }

    /**
     * Returns the rotation angle of transform . Measured in standard math polar coordinates system.
     * @return
     */
    public double getRotation() {
        double rot = rotation + (getGameObject().getParent() != null ? getGameObject().getParent().getTransform().getRotation() : 0);
        if(rot > Math.toRadians(360))
            rot -= Math.toRadians(360);
        else if(rot < 0)
            rot += Math.toRadians(360);
        return rot;
    }

    /**
     * Returns the scale of transform in pixels (Not ratio).
     * @return
     */
    public Vector2D getScale() {
        return scale;
    }

    /**
     * Returns the location offset of pivot point from center point of transform.
     * @return
     */
    public Vector2D getPivot() {
        return pivot;
    }


    public void setLocation(Vector2D location) {
        this.location = location;
    }

    public void setRotation(double rotation) {
        this.rotation = rotation;
    }

    public void setScale(Vector2D scale) {
        this.scale = scale;
    }

    public void setPivot(Vector2D pivot) {
        this.pivot = pivot;
    }


    /**
     * Returns the location of center point of this transform.
     * @return
     */
    public Vector2D getCenter(){
        Vector2D ul = getUpperLeft();
        Vector2D lr = getLowerRight();
        return new Vector2D((ul.getX()+lr.getX()) / 2,(ul.getY()+lr.getY()) / 2);
    }

    /**
     * Returns the location of center point of this transform.
     * @return
     */
    public Vector2D getCenter(boolean onScreenLocation){
        Vector2D ul = getUpperLeft(onScreenLocation);
        Vector2D lr = getLowerRight(onScreenLocation);
        return new Vector2D((ul.getX()+lr.getX()) / 2,(ul.getY()+lr.getY()) / 2);
    }

    /**
     * Returns the location of upper-left point of this transform.
     * @return
     */
    public Vector2D getUpperLeft(){
        double tcos = Math.cos(getRotation());
        double tsin = Math.sin(getRotation());
        return new Vector2D(-pivot.getX()*tcos - (scale.getX()/2) * tcos - pivot.getY() * tsin - (scale.getY()/2)*tsin + getLocation().getX() , pivot.getX()*tsin + (scale.getX()/2)*tsin - pivot.getY()*tcos - (scale.getY()/2)*tcos + getLocation().getY());
    }

    /**
     * Returns the location of upper-left point of this transform.
     * @return
     */
    public Vector2D getUpperLeft(boolean onScreenLocation){
        double tcos = Math.cos(getRotation());
        double tsin = Math.sin(getRotation());
        return new Vector2D(-pivot.getX()*tcos - (scale.getX()/2) * tcos - pivot.getY() * tsin - (scale.getY()/2)*tsin + getLocation(onScreenLocation).getX() , pivot.getX()*tsin + (scale.getX()/2)*tsin - pivot.getY()*tcos - (scale.getY()/2)*tcos + getLocation(onScreenLocation).getY());
    }

    /**
     * Returns the location of upper-right point of this transform.
     * @return
     */
    public Vector2D getUpperRight(){
        double tcos = Math.cos(getRotation());
        double tsin = Math.sin(getRotation());
        return new Vector2D(-pivot.getX()*tcos + (scale.getX()/2) * tcos - pivot.getY() * tsin - (scale.getY()/2)*tsin + getLocation().getX() , pivot.getX()*tsin - (scale.getX()/2)*tsin - pivot.getY()*tcos - (scale.getY()/2)*tcos + getLocation().getY());
    }

    /**
     * Returns the location of upper-right point of this transform.
     * @return
     */
    public Vector2D getUpperRight(boolean onScreenLocation){
        double tcos = Math.cos(getRotation());
        double tsin = Math.sin(getRotation());
        return new Vector2D(-pivot.getX()*tcos + (scale.getX()/2) * tcos - pivot.getY() * tsin - (scale.getY()/2)*tsin + getLocation(onScreenLocation).getX() , pivot.getX()*tsin - (scale.getX()/2)*tsin - pivot.getY()*tcos - (scale.getY()/2)*tcos + getLocation(onScreenLocation).getY());
    }

    /**
     * Returns the location of lower-left point of this transform.
     * @return
     */
    public Vector2D getLowerLeft(){
        double tcos = Math.cos(getRotation());
        double tsin = Math.sin(getRotation());
        return new Vector2D(-pivot.getX()*tcos - (scale.getX()/2) * tcos - pivot.getY() * tsin + (scale.getY()/2)*tsin + getLocation().getX() , pivot.getX()*tsin + (scale.getX()/2)*tsin - pivot.getY()*tcos + (scale.getY()/2)*tcos + getLocation().getY());
    }

    /**
     * Returns the location of lower-left point of this transform.
     * @return
     */
    public Vector2D getLowerLeft(boolean onScreenLocation){
        double tcos = Math.cos(getRotation());
        double tsin = Math.sin(getRotation());
        return new Vector2D(-pivot.getX()*tcos - (scale.getX()/2) * tcos - pivot.getY() * tsin + (scale.getY()/2)*tsin + getLocation(onScreenLocation).getX() , pivot.getX()*tsin + (scale.getX()/2)*tsin - pivot.getY()*tcos + (scale.getY()/2)*tcos + getLocation(onScreenLocation).getY());
    }


    /**
     * Returns the location of lower-right point of this transform.
     * @return
     */
    public Vector2D getLowerRight(){
        double tcos = Math.cos(getRotation());
        double tsin = Math.sin(getRotation());
        return new Vector2D(-pivot.getX()*tcos + (scale.getX()/2) * tcos - pivot.getY() * tsin + (scale.getY()/2)*tsin + getLocation().getX() , pivot.getX()*tsin - (scale.getX()/2)*tsin - pivot.getY()*tcos + (scale.getY()/2)*tcos + getLocation().getY());
    }

    /**
     * Returns the location of lower-right point of this transform.
     * @return
     */
    public Vector2D getLowerRight(boolean onScreenLocation){
        double tcos = Math.cos(getRotation());
        double tsin = Math.sin(getRotation());
        return new Vector2D(-pivot.getX()*tcos + (scale.getX()/2) * tcos - pivot.getY() * tsin + (scale.getY()/2)*tsin + getLocation(onScreenLocation).getX() , pivot.getX()*tsin - (scale.getX()/2)*tsin - pivot.getY()*tcos + (scale.getY()/2)*tcos + getLocation(onScreenLocation).getY());
    }

    /**
     * Returns half of distance between upper-left point and lower-right point of transform.
     * @return
     */
    public double getDiameter(){
        return Math.sqrt(scale.getX()*scale.getX() + scale.getY()*scale.getY())/2;
    }

    /**
     * Returns a unit vector which is in forward direction of transform.
     * @return
     */
    public Vector2D forward(){
//        Vector2D ul = getUpperLeft();
//        Vector2D ur = getUpperRight();
//        Vector2D fVector = new Vector2D(ur.getX() - ul.getX(),ur.getY() - ul.getY());
//        return fVector.div(fVector.size());
        double tcos = Math.cos(getRotation());
        double tsin = Math.sin(getRotation());
        return new Vector2D(tcos,-tsin);
    }

    /**
     * Returns a unit vector which is in right direction of transform.
     * @return
     */
    public Vector2D right(){
//        Vector2D ul = getUpperLeft();
//        Vector2D ll = getLowerLeft();
//        Vector2D rVector = new Vector2D(ll.getX() - ul.getX(),ll.getY() - ul.getY());
//        return rVector.div(rVector.size());
        double tcos = Math.cos(getRotation());
        double tsin = Math.sin(getRotation());
        return new Vector2D(tsin,tcos);
    }

    /**
     * Static method used to calculate rotation angle offset which is needed for t1 transform to look at t2.
     * @param t1 Who is looking at t2.
     * @param t2 A 2D vector which represents the location of a point.
     * @return Rotation offset needed for t1 transform.
     */
    public static double lookAt(Transform t1,Vector2D t2){
        double rotAngle = Math.atan2(t1.getLocation().getY() - t2.getY(),t2.getX() - t1.getLocation().getX());
        return rotAngle - t1.getRotation();
    }

}
