package ir.aut.GameEngine.Components.Animation;

import ir.aut.GameEngine.Scene;
import ir.aut.GameEngine.Vector.Vector2D;

public class RotationAnimator extends Animator {

    private double firstAngle , secondAngle;

    /**
     * Creates a new Animator component with given properties.
     *
     * @param secondAngle Destination angle.
     * @param duration Duration of animation in milliseconds.
     * @param repeat   Initially sets repeat mode (can be configured later).
     */
    public RotationAnimator(double secondAngle, long duration, boolean repeat) {
        super(duration, repeat);
        this.secondAngle = secondAngle;
    }

    public double getFirstAngle() {
        return firstAngle;
    }

    public void setFirstAngle(double firstAngle) {
        this.firstAngle = firstAngle;
        innerPlay();
    }

    public double getSecondAngle() {
        return secondAngle;
    }

    public void setSecondAngle(double secondAngle) {
        this.secondAngle = secondAngle;
        innerPlay();
    }

    @Override
    protected void innerPlay() {
        firstAngle = getGameObject().getTransform().getRotation();
    }

    @Override
    protected void innerPause() {

    }

    @Override
    protected void innerResume() {

    }

    @Override
    protected void onAnimationUpdate() {
        double currentRatio = (((double)(System.currentTimeMillis() - startTime)) / (duration));
        if(currentRatio==0)
        System.out.println(currentRatio);
        if(currentRatio >= 1) {
            if(!repeat) {
                isPlaying = false;
                if (animationHandler!=null && animationHandler.animationFinished()) {
                    getGameObject().getTransform().rotate(firstAngle - getGameObject().getTransform().getRotation());
                }
            } else {
                getGameObject().getTransform().rotate(firstAngle - getGameObject().getTransform().getRotation());
                startTime = System.currentTimeMillis();
            }
        }
        else
            rotate(currentRatio*(secondAngle-firstAngle) + firstAngle - getGameObject().getTransform().getRotation());
    }

    private void rotate(double tetha){
        if(getGameObject().getCollider()!=null)
            getGameObject().getCollider().rotate(tetha);
        else
            getGameObject().getTransform().rotate(tetha);
    }
}
