package ir.aut.GameEngine.Components.Animation;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;

public class SpriteAnimator extends Animator{

    private ArrayList<BufferedImage> sprites = new ArrayList<>() ;
    private BufferedImage backupSprite;

    /**
     * Creates a new SpriteAnimator component with given properties.
     *
     * @param spritesDirAddress /path/to/directory , in which , there are some numbered images (beginning from 0.png) with png format.<br> Note : Don't type slash after last directory's name.
     * @param count             Number of images for animation.
     * @param duration          Duration of animation in milliseconds.
     * @param repeat            Initially sets repeat mode (can be configured later).
     */
    public SpriteAnimator(String spritesDirAddress, int count, long duration, boolean repeat) {
        super(duration, repeat);
        loadSprites(spritesDirAddress,count);
    }

    private void loadSprites(String spritesDirAddress,int count){
        isPlaying = false;
        for (int i = 0; i < count; i++) {
            try {
                sprites.add(ImageIO.read(getClass().getResource(spritesDirAddress + "/" + i + ".png")));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void innerPlay() {
        backupSprite = getGameObject().getRenderer().getSprite();
    }

    @Override
    protected void innerPause() {

    }

    @Override
    protected void innerResume() {

    }

    @Override
    protected void onAnimationUpdate() {
        int currentSpriteIndex = (int) ((System.currentTimeMillis() - startTime) / (duration / sprites.size()));
        if(currentSpriteIndex >= sprites.size()) {
            if(!repeat) {
                isPlaying = false;
                if (animationHandler!=null && animationHandler.animationFinished()) {
                    getGameObject().getRenderer().setSprite(backupSprite);
                }
            } else {
                startTime = System.currentTimeMillis();
            }
        }
        else
            getGameObject().getRenderer().setSprite(sprites.get((playDirection ? currentSpriteIndex : sprites.size() - 1 - currentSpriteIndex)));
    }
}
