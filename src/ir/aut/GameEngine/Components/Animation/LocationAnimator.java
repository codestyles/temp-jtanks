package ir.aut.GameEngine.Components.Animation;

import ir.aut.GameEngine.Scene;
import ir.aut.GameEngine.Vector.Vector2D;


public class LocationAnimator extends Animator{

    private Vector2D loc;
    private Vector2D firstLoc;
    private double speed;
    private double finishDistance;

    private Vector2D direction;


    /**
     * Creates a new SpriteAnimator component with given properties.
     *
     * @param loc Location of destination.
     * @param finishDistance Distance between loc and firstLoc to finish animation.
     * @param speed          Speed of animation.
     * @param repeat            Initially sets repeat mode (can be configured later).
     */
    public LocationAnimator(Vector2D loc , double finishDistance, double speed, boolean repeat) {
        super(0, repeat);
        this.speed = speed;
        this.loc = loc;
        this.finishDistance = finishDistance;
    }

    public Vector2D getLoc() {
        return loc;
    }

    public void setLoc(Vector2D loc) {
        this.loc = loc;
        innerPlay();
    }

    public Vector2D getFirstLoc() {
        return firstLoc;
    }

    public void setFirstLoc(Vector2D firstLoc) {
        this.firstLoc = firstLoc;
        innerPlay();
    }

    @Override
    protected void innerPlay() {
        firstLoc = getGameObject().getTransform().getLocation();
        direction = Vector2D.add(loc, firstLoc.negative());
        direction = direction.div(direction.size());
    }

    @Override
    protected void innerPause() {

    }

    @Override
    protected void innerResume() {

    }

    @Override
    protected void onAnimationUpdate() {
        if(Vector2D.distance(getGameObject().getTransform().getLocation(),loc) < finishDistance){
            // Finished !
            move(Vector2D.distance(getGameObject().getTransform().getLocation(),loc));
            if(!repeat) {
                isPlaying = false;
                if (animationHandler!=null && animationHandler.animationFinished()) {
                    move(-Vector2D.distance(getGameObject().getTransform().getLocation(), firstLoc));
                }
            } else {
                move(-Vector2D.distance(getGameObject().getTransform().getLocation(), firstLoc));
                startTime = System.currentTimeMillis();
            }
        } else {
            move(Scene.deltaTime * speed);
        }
    }

    private void move(double d){
        if(getGameObject().getCollider()!=null)
            getGameObject().getCollider().move(direction.mul(d));
        else
            getGameObject().getTransform().move(direction.mul(d));
    }
}
