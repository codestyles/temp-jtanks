package ir.aut.GameEngine.Components.Animation;

/**
 * <h1>Animator Events</h1>
 * methods which is called according to some animator events and actions.
 *
 * <h2>What should we do here ?</h2>
 * Nothing .
 *
 * @author MMKH
 *
 */
public interface AnimationHandler {

    /**
     * Runs when {@code Animator} component finishes it's non-looping animation.
     * @return {@code true} if you want to restore original sprite of gameObject (after animation finishes), {@code false} if not .
     */
    boolean animationFinished();
}
