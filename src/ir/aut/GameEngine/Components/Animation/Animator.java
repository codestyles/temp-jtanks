package ir.aut.GameEngine.Components.Animation;

import ir.aut.GameEngine.Components.Component;

import java.awt.*;

/**
 * <h1>Animation Player Component</h1>
 * A component which plays some sprite-made animation on a gameObject.
 *
 * <h2>What should we do here ?</h2>
 * Nothing , but for using this component , just create an instance of it and assign it to your gameObject's {@code animator} field.<br>
 *
 * @author MMKH
 *
 */
public abstract class Animator extends Component {

//    private ArrayList<BufferedImage> sprites = new ArrayList<>() ;
    protected long duration;
    protected boolean repeat;

    protected boolean isPlaying;
    protected long startTime;
    protected boolean playDirection;
    protected AnimationHandler animationHandler;
//    private BufferedImage backupSprite;

    /**
     * Creates a new Animator component with given properties.
     * @param duration Duration of animation in milliseconds.
     * @param repeat Initially sets repeat mode (can be configured later).
     */
    public Animator(long duration , boolean repeat) {
        this.duration = duration;
        this.repeat = repeat;
        isPlaying=false;
//        loadSprites(spritesDirAddress,count);
    }

//    private void loadSprites(String spritesDirAddress,int count){
//        isPlaying = false;
//        for (int i = 0; i < count; i++) {
//            try {
//                sprites.add(ImageIO.read(getClass().getResource(spritesDirAddress + "/" + i + ".png")));
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//    }

    /**
     * Plays this animator object with given direction . If this is a non-looping animation , then the single method of {@code animationHandler} is called at the end of animation.
     * @param fwd Is play direction "Forward" ?
     * @param animationHandler An {@code AnimationHandler} instance which have a single method in which you should return {@code true} if you want to restore original sprite of gameObject (after animation finishes), or return {@code false} if not .
     */
    public void play(boolean fwd,AnimationHandler animationHandler){
        this.animationHandler = animationHandler;
        playDirection = fwd;
        isPlaying = true;
        innerPlay();
        startTime = System.currentTimeMillis();

    }

    protected abstract void innerPlay();
    protected abstract void innerPause();
    protected abstract void innerResume();
    protected abstract void onAnimationUpdate();

    /**
     * Pauses or stops the current playing animation (if there is any!).
     */
    public void pause(){
        isPlaying = false;
        innerPause();
        startTime -= System.currentTimeMillis();
    }

    /**
     * Resumes the current playing animation.<br>
     * <font color="red">Warning : If this method is called before {@code play(...)} and {@code pause(...)} , performed actions are undefined.</font>
     */
    public void resume(){
        startTime += System.currentTimeMillis();
        innerResume();
        isPlaying=true;
    }

    public long getDuration() {
        return duration;
    }

    public boolean isPlaying() {
        return isPlaying;
    }

    /**
     * Returns {@code true} if play direction is "Forward" , else returns {@code false}.
     * @return
     */
    public boolean isPlayDirection() {
        return playDirection;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public boolean isRepeat() {
        return repeat;
    }

    public void setRepeat(boolean repeat) {
        this.repeat = repeat;
    }

    /**
     * Not used.
     */
    @Override
    public void start() {

    }

    /**
     * Internal actions for playing animation.
     */
    @Override
    public void update() {
        if (isPlaying){
            onAnimationUpdate();
        }
    }

    /**
     * Not used.
     * @param g Graphics2D object for drawing on screen.
     */
    @Override
    public void onGUI(Graphics2D g) {

    }
}
