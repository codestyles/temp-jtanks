package ir.aut.GameEngine.Components.Audio;

/**
 * <h1>SoundPlayer Events</h1>
 * methods which is called according to some sound player events and actions.
 *
 * <h2>What should we do here ?</h2>
 * Nothing .
 *
 * @author MMKH
 *
 */
public interface SoundFinishHandler {

    /**
     * Runs when {@code SoundPlayer} component finishes playing it's non-looping sound.
     */
    void soundFinished();
}
