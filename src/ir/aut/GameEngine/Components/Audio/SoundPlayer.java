package ir.aut.GameEngine.Components.Audio;

import ir.aut.GameEngine.Components.Component;
import ir.aut.GameEngine.Scene;
import ir.aut.GameEngine.Vector.Vector2D;

import javax.sound.sampled.*;
import java.awt.*;
import java.io.IOException;

/**
 * <h1>Sound Player Component</h1>
 * A component which plays some sound on a gameObject which is begin listened by {@code soundListener} gameObject of {@code Scene}.
 *
 * <h2>What should we do here ?</h2>
 * Nothing , but for using this component , just create an instance of it and assign it to your gameObject's {@code soundPlayer} field.<br>
 *
 * @author MMKH
 *
 */
public final class SoundPlayer extends Component {

    private Clip clip;
    // volume - Between 0 and 30 , Or DYNAMIC_VOLUME (-1)
    private int volume;
    private boolean repeat;
    private boolean isPlaying;

    /**
     * Volume is automatically determined according to gameObject's distance from {@code soundListener}.
     */
    public static final int DYNAMIC_VOLUME = -1;

    /**
     * Creates a new SoundPlayer component with given properties.
     * @param soundAddress /path/to/sound.wav
     * @param repeat Initially sets repeat mode (can be configured later).
     * @param volume An int number between 0 and 30 , Or {@code DYNAMIC_VOLUME}.
     *
     * @throws RuntimeException If the given volume isn't in legal bounds.
     */
    public SoundPlayer(String soundAddress, boolean repeat,int volume) {
        if(volume < -1 || volume > 30){
            throw new RuntimeException("Volume must be between 0 and 30 , Or DYNAMIC_VOLUME (-1)");
        }
        this.repeat = repeat;
        this.volume = volume;
        isPlaying = false;
        loadClip(soundAddress);

    }

    private void loadClip(String soundAddress){

        if(clip != null && clip.isOpen()) {
            clip.stop();
            clip.close();
        }

        try {
            clip = AudioSystem.getClip();
            clip.open(AudioSystem.
                    getAudioInputStream( getClass().getResource(soundAddress) ));
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Plays sound . If this is a non-looping sound , then the single method of {@code soundFinishHandler} is called at the end of playing.
     * @param soundFinishHandler
     */
    public void play(SoundFinishHandler soundFinishHandler){
        if(volume != DYNAMIC_VOLUME)
            ((FloatControl)clip.getControl(FloatControl.Type.MASTER_GAIN)).setValue(-25f + volume);
        else
            ((FloatControl)clip.getControl(FloatControl.Type.MASTER_GAIN)).setValue(-25f + 7500f/(Math.max(300f,(float)Vector2D.distance(Scene.getInstance().getSoundListener().getTransform().getLocation(),getGameObject().getTransform().getLocation()))));
        clip.addLineListener((event -> {
            if (event.getType() == LineEvent.Type.STOP) {
                if(!repeat) {
                    isPlaying=false;
                    if (soundFinishHandler != null)
                        soundFinishHandler.soundFinished();
                }
                else {
                    if(volume != DYNAMIC_VOLUME)
                        ((FloatControl)clip.getControl(FloatControl.Type.MASTER_GAIN)).setValue(-25f + volume);
                    else
                        ((FloatControl)clip.getControl(FloatControl.Type.MASTER_GAIN)).setValue(-25f + 7500f/(Math.max(300f,(float)Vector2D.distance(Scene.getInstance().getSoundListener().getTransform().getLocation(),getGameObject().getTransform().getLocation()))));
                    clip.setFramePosition(0);
                    clip.start();
                }
            }
        }));
        isPlaying=true;
        clip.setFramePosition(0);
        clip.start();
    }

    /**
     * Stops (not pause) the current playing sound (if there is any!).
     */
    public void stop(){
        isPlaying=false;
        clip.stop();
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public boolean isRepeat() {
        return repeat;
    }

    public void setRepeat(boolean repeat) {
        this.repeat = repeat;
    }

    public boolean isPlaying() {
        return isPlaying;
    }

    /**
     * Not used.
     */
    @Override
    public void start() {

    }

    /**
     * Not used.
     */
    @Override
    public void update() {

    }

    /**
     * Not used.
     * @param g Graphics2D object for drawing on screen.
     */
    @Override
    public void onGUI(Graphics2D g) {

    }
}
