package ir.aut.GameEngine.Components;

import ir.aut.GameEngine.Core.GameEvents;
import ir.aut.GameEngine.Core.GameObject;

/**
 * <h1>Component of a game object (Must inherit)</h1>
 * An abstract class that represents a component which should be added to game objects.<br>
 *
 * <h2>What should we do here ?</h2>
 * Nothing here , but for adding a new Component type to game , follow these instructions :<br>
 * <p>
 *     1. Create a class , which extends {@code Component} class.<br>
 *     2. So , there should be three implementing methods ({@code start} , {@code update} and {@code onGUI}).<br>
 *     3. Feel free to add some extra fields and methods and even , constructors.<br>
 *     Finished ! Now , don't forget to create and add components from you Component extended class in constructor of your GameObject extended class.<br>
 * </p>
 *
 * @author MMKH
 *
 */
public abstract class Component implements GameEvents {

    private GameObject gameObject;
    private boolean isActive = true;

    /**
     * Returns the {@code GameObject} which owns this Component.
     * @return
     */
    public GameObject getGameObject() {
        return gameObject;
    }

    /**
     * Sets the owner of this component.
     * @param gameObject
     */
    public void setGameObject(GameObject gameObject) {
        this.gameObject = gameObject;
    }

    /**
     * Is this component active ?<br>
     * When a component isn't active , it's {@code GameEvents} methods doesn't run.
     * @return
     */
    public boolean isActive() {
        return isActive;
    }

    /**
     * Activates or deactivates this component.
     * @param active
     */
    public void setActive(boolean active) {
        isActive = active;
    }
}
