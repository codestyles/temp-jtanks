package ir.aut.GameEngine.Components.Rendering;

import ir.aut.GameEngine.Components.Component;
import ir.aut.GameEngine.Scene;
import ir.aut.GameEngine.Vector.Vector2D;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

/**
 * <h1>Component which draws gameObjects on screen</h1>
 * A class that represents a SpriteRenderer component , which draws gameObjects on screen for each frame.<br>
 *
 * <h2>What should we do here ?</h2>
 * Nothing , but for using this component , just create an instance of it and assign it to your gameObject's {@code renderer} field.<br>
 *
 * @author MMKH
 *
 */
public final class SpriteRenderer extends Component {

    private BufferedImage sprite;

    private boolean inCameraFieldOfView=true;

    private boolean isStatic = false;

    public void setSprite(BufferedImage sprite) {
        this.sprite = sprite;
    }

    public BufferedImage getSprite() {
        return sprite;
    }

    public boolean isStatic() {
        return isStatic;
    }

    public void setStatic(boolean aStatic) {
        isStatic = aStatic;
    }

    /**
     * Not used.
     */
    @Override
    public void start() {
    }

    /**
     * Not used.
     */
    @Override
    public void update() {

    }

    /**
     * Draws gameObject on screen with {@code sprite} image and it's transform information.<br>
     * <font color="blue">Note : If {@code sprite} equals {@code null} then nothing is drawn.</font><br>
     * Pivot point is magenta ,<br>
     * Center point is green ,<br>
     * Sprite local coordinates system lines are in red ,<br>
     * Local upper-left point of sprite is in blue .
     *
     * @param g Graphics2D object for drawing on screen.
     */
    @Override
    public void onGUI(Graphics2D g) {
        if(sprite!=null) {
            AffineTransform at = new AffineTransform();

            Vector2D upperLeft = getGameObject().getTransform().getUpperLeft(true);
            double doubleDiameter = 2 * getGameObject().getTransform().getDiameter();
            if(!(getGameObject().getTransform().getLocation(true).getX() + doubleDiameter < 0 || getGameObject().getTransform().getLocation(true).getY() + doubleDiameter < 0 || getGameObject().getTransform().getLocation(true).getX() - doubleDiameter > Scene.GAME_WIDTH || getGameObject().getTransform().getLocation(true).getY() - doubleDiameter > Scene.GAME_HEIGHT)) {
                inCameraFieldOfView=true;
                Scene.renderingObjectsCount++;
                if(!isStatic) {
                    at.translate(-(getGameObject().getTransform().getScale().getX() / 2 + getGameObject().getTransform().getPivot().getX()), -(getGameObject().getTransform().getScale().getY() / 2 + getGameObject().getTransform().getPivot().getY()));
                    at.rotate(-getGameObject().getTransform().getRotation(), (getGameObject().getTransform().getScale().getX() / 2 + getGameObject().getTransform().getPivot().getX()), (getGameObject().getTransform().getScale().getY() / 2 + getGameObject().getTransform().getPivot().getY()));
                    at.translate((getGameObject().getTransform().getScale().getX() / 2 + getGameObject().getTransform().getPivot().getX()), (getGameObject().getTransform().getScale().getY() / 2 + getGameObject().getTransform().getPivot().getY()));
                    at.scale(getGameObject().getTransform().getScale().getX() / sprite.getWidth(), getGameObject().getTransform().getScale().getY() / sprite.getHeight());

                    double xOffset, yOffset;

                    if (getGameObject().getTransform().getRotation() >= 0 && getGameObject().getTransform().getRotation() < Math.toRadians(90)) {
                        // Q1
                        xOffset = 0;
                        yOffset = upperLeft.getY() - getGameObject().getTransform().getUpperRight(true).getY();
                        //System.out.println("1");
                    } else if (getGameObject().getTransform().getRotation() >= Math.toRadians(90) && getGameObject().getTransform().getRotation() < Math.toRadians(180)) {
                        // Q2
                        xOffset = upperLeft.getX() - getGameObject().getTransform().getUpperRight(true).getX();
                        yOffset = upperLeft.getY() - getGameObject().getTransform().getLowerRight(true).getY();
                        // System.out.println("2");
                    } else if (getGameObject().getTransform().getRotation() >= Math.toRadians(180) && getGameObject().getTransform().getRotation() < Math.toRadians(270)) {
                        // Q3
                        xOffset = upperLeft.getX() - getGameObject().getTransform().getLowerRight(true).getX();
                        yOffset = upperLeft.getY() - getGameObject().getTransform().getLowerLeft(true).getY();
                        // System.out.println("3");
                    } else {
                        // Q4
                        xOffset = upperLeft.getX() - getGameObject().getTransform().getLowerLeft(true).getX();
                        yOffset = 0;
                        //  System.out.println("4");
                    }

                    double tsin = Math.sin(getGameObject().getTransform().getRotation());
                    double tcos = Math.cos(getGameObject().getTransform().getRotation());
                    at.translate((xOffset * tcos - yOffset * tsin) / (getGameObject().getTransform().getScale().getX() / sprite.getWidth()), ((yOffset) * tcos + xOffset * tsin) / (getGameObject().getTransform().getScale().getY() / sprite.getHeight()));

                    // Drawing debugging information
                    if (Scene.debuggingMode) {
                        Vector2D point = getGameObject().getTransform().getLocation(true);
                        g.setColor(Color.MAGENTA);
                        g.fillRect((int) point.getX() - 5, (int) point.getY() - 5, 10, 10);
                        g.setColor(Color.GREEN);
                        point = getGameObject().getTransform().getCenter(true);
                        g.fillRect((int) point.getX() - 2, (int) point.getY() - 2, 6, 6);
                        g.setColor(Color.RED);
                        g.fillRect((int) upperLeft.getX() - (int) xOffset, (int) upperLeft.getY() - (int) yOffset, 5, 5);
                        g.drawLine((int) upperLeft.getX() - (int) xOffset, (int) upperLeft.getY() - (int) yOffset, (int) upperLeft.getX() - (int) xOffset + 200, (int) upperLeft.getY() - (int) yOffset);
                        g.drawLine((int) upperLeft.getX() - (int) xOffset, (int) upperLeft.getY() - (int) yOffset, (int) upperLeft.getX() - (int) xOffset, (int) upperLeft.getY() - (int) yOffset + 200);
                        g.setColor(Color.BLUE);
                        g.fillRect((int) upperLeft.getX(), (int) upperLeft.getY(), 5, 5);
                    }

                    // Drawing sprite with configured transforms
                    try {
                        g.drawImage(new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR).filter(sprite, null), (int) upperLeft.getX() - (int) xOffset, (int) upperLeft.getY() - (int) yOffset, null);
                    } catch (Exception e) {
                    }
                } else {
                    try {
                        g.drawImage(sprite, (int) upperLeft.getX() , (int) upperLeft.getY() , null);
                    } catch (Exception e) {
                    }
                }
            }
            else {
                inCameraFieldOfView = false;
            }
        }

    }

    public boolean isInCameraFieldOfView() {
        return inCameraFieldOfView;
    }
}
