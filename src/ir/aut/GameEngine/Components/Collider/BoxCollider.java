package ir.aut.GameEngine.Components.Collider;

import ir.aut.GameEngine.Components.Component;
import ir.aut.GameEngine.Components.Transform;
import ir.aut.GameEngine.Core.GameObject;
import ir.aut.GameEngine.Scene;
import ir.aut.GameEngine.Vector.Vector2D;

import java.awt.*;
import java.awt.geom.Line2D;
import java.util.ArrayList;


/**
 * <h1>Component which manages collisions</h1>
 * A class that represents a BoxCollider component , which manages collisions made by moving , rotating or scaling Transforms<br>
 *
 * <h2>What should we do here ?</h2>
 * Nothing , but for using this component , just create an instance of it and assign it to your gameObject's {@code collider} field.<br>
 *
 * @author MMKH
 *
 */
public final class BoxCollider extends Component implements CollisionHandler {


    /**
     * If a collider is trigger , then it doesn't prevent other colliders to collide with it ; but it just raises {@code onTriggerHit(...)} event method on two colliding and triggering gameObjects (Else , it raises {@code onColliderHit(...)} event method).
     */
    private boolean isTrigger;

    /**
     * Not used.
     */
    @Override
    public void start() {

    }

    /**
     * Not used.
     */
    @Override
    public void update() {

    }

    /**
     * Used just for printing debugging information.<br>
     * Collider bounds are in green.<br>
     * @param g Graphics2D object for drawing on screen.
     */
    @Override
    public void onGUI(Graphics2D g) {
        if(Scene.debuggingMode) {
            Transform t1 = getGameObject().getTransform();
            Vector2D upperLeft1 = t1.getUpperLeft(), upperRight1 = t1.getUpperRight(), lowerLeft1 = t1.getLowerLeft(), lowerRight1 = t1.getLowerRight();
            Line2D[] lines = new Line2D[]{Vector2D.line(upperLeft1, lowerLeft1), Vector2D.line(upperLeft1, upperRight1), Vector2D.line(lowerLeft1, lowerRight1), Vector2D.line(upperRight1, lowerRight1)};

            for (Line2D line2D : lines) {
                g.setColor(Color.GREEN);
                g.drawLine((int) line2D.getX1(), (int) line2D.getY1(), (int) line2D.getX2(), (int) line2D.getY2());
            }
        }
    }

    @Override
    public void OnTriggerHit(GameObject other) {
        for(Component component : getGameObject().getComponents())
            if(component instanceof CollisionHandler){
                ((CollisionHandler)component).OnTriggerHit(other);
            }
    }

    @Override
    public void OnColliderHit(GameObject other) {
        for(Component component : getGameObject().getComponents())
            if(component instanceof CollisionHandler){
                ((CollisionHandler)component).OnColliderHit(other);
            }
    }

    /**
     * If a collider is trigger , then it doesn't prevent other colliders to collide with it ; but it just raises {@code onTriggerHit(...)} event method on two colliding and triggering gameObjects (Else , it raises {@code onColliderHit(...)} event method).
     * @return
     */
    public boolean isTrigger() {
        return isTrigger;
    }

    /**
     * If a collider is trigger , then it doesn't prevent other colliders to collide with it ; but it just raises {@code onTriggerHit(...)} event method on two colliding and triggering gameObjects (Else , it raises {@code onColliderHit(...)} event method).
     * @param trigger
     */
    public void setTrigger(boolean trigger) {
        isTrigger = trigger;
    }

    /**
     * Moves transform of this gameObject by given location offset if it could , according to collisions.<br>
     * Compare with {@code Transform.move(...)}
     * @param locationOffset
     */
    public void move(Vector2D locationOffset){
        if(!locationOffset.isZeroVector()) {
            Transform newTransform = new Transform(getGameObject().getTransform());
            newTransform.move(locationOffset);

            if (!applyAnyHit(newTransform))
                getGameObject().getTransform().move(locationOffset);
        }
    }

    /**
     * Rotates transform of this gameObject by given rotation offset if it could , according to collisions.<br>
     * Compare with {@code Transform.rotate(...)}
     * @param rotationOffset (In radians)
     */
    public void rotate(double rotationOffset){
        if(rotationOffset!=0) {
            Transform newTransform = new Transform(getGameObject().getTransform());
            newTransform.rotate(rotationOffset);

            if (!applyAnyHit(newTransform))
                getGameObject().getTransform().rotate(rotationOffset);
        }
    }

    /**
     * Scales transform of this gameObject by given scale offset if it could , according to collisions.<br>
     * Compare with {@code Transform.scale(...)}
     * @param scaleOffset (In pixels, not ratio)
     */
    public void scale(Vector2D scaleOffset){
        if(!scaleOffset.isZeroVector()) {
            Transform newTransform = new Transform(getGameObject().getTransform());
            newTransform.scale(scaleOffset);

            if (!applyAnyHit(newTransform))
                getGameObject().getTransform().scale(scaleOffset);
        }
    }

    /**
     * Checks if the given transform collides with any other colliders.
     * @param t1
     * @return {@code true} if it collides (not triggers) , {@code false} if not.
     */
    private boolean applyAnyHit(Transform t1){
        ArrayList<BoxCollider> triggerHits = new ArrayList<>();
        ArrayList<BoxCollider> colliderHits = new ArrayList<>();
        double diameter = t1.getDiameter();
        Vector2D centerPoint = t1.getCenter();
        Vector2D upperLeft1 = t1.getUpperLeft(), upperRight1 = t1.getUpperRight(), lowerLeft1 = t1.getLowerLeft(), lowerRight1 = t1.getLowerRight();
        Line2D[] lines1 = new Line2D[]{Vector2D.line(upperLeft1, lowerLeft1), Vector2D.line(upperLeft1, upperRight1), Vector2D.line(lowerLeft1, lowerRight1), Vector2D.line(upperRight1, lowerRight1)};

        for (GameObject otherGameObject : Scene.getInstance().getGameObjects()) {
            if (otherGameObject.isActive() && otherGameObject.getCollider() != null && otherGameObject!=getGameObject()) {
                if (Vector2D.distance(centerPoint, otherGameObject.getTransform().getCenter()) <= diameter + otherGameObject.getTransform().getDiameter()) {
                    if (checkCollisionOfLines(lines1, otherGameObject.getTransform()))
                        if(!isTrigger && !otherGameObject.getCollider().isTrigger)
                            colliderHits.add(otherGameObject.getCollider());
                        else
                            triggerHits.add(otherGameObject.getCollider());
                }
            }
        }

        for(BoxCollider collider : triggerHits){
            OnTriggerHit(collider.getGameObject());
            collider.OnTriggerHit(getGameObject());
        }
        for(BoxCollider collider : colliderHits){
            OnColliderHit(collider.getGameObject());
            collider.OnColliderHit(getGameObject());
        }

        return !colliderHits.isEmpty();
    }

    /**
     * Checks collision between given lines and bounds of given transform
     * @param lines1
     * @param t2
     * @return
     */
    private boolean checkCollisionOfLines(Line2D[] lines1,Transform t2){
        Vector2D upperLeft2 = t2.getUpperLeft() , upperRight2 = t2.getUpperRight() , lowerLeft2 = t2.getLowerLeft() , lowerRight2 = t2.getLowerRight();
        Line2D[] lines2 = new Line2D[]{ Vector2D.line(upperLeft2,lowerLeft2) ,Vector2D.line(upperLeft2,upperRight2) ,  Vector2D.line(lowerLeft2,lowerRight2) , Vector2D.line(upperRight2,lowerRight2)};

        for(Line2D firstLine : lines1)
            for(Line2D secondLine : lines2)
                if(firstLine.intersectsLine(secondLine))
                    return true;

        return false;
    }

}
