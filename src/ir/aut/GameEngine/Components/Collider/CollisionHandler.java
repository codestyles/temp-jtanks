package ir.aut.GameEngine.Components.Collider;

import ir.aut.GameEngine.Core.GameObject;

/**
 * <h1>Collider Events</h1>
 * methods which is called according to some sound collision events and actions.
 *
 * <h2>What should we do here ?</h2>
 * Nothing .
 *
 * @author MMKH
 *
 */
public interface CollisionHandler {
    void OnTriggerHit(GameObject other);
    void OnColliderHit(GameObject other);
}
