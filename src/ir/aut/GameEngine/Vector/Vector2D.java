package ir.aut.GameEngine.Vector;


import java.awt.geom.Line2D;

/**
 * <h1>Vector in two dimension</h1>
 * A helper class that represents a 2D vector in screen<br>
 *
 * <h2>What should we do here ?</h2>
 * Nothing , this is an helper class.
 *
 * @author MMKH
 *
 */
public final class Vector2D {
    private double x;
    private double y;

    /**
     * Creates a new 2D vector.
     * @param x X coordinate
     * @param y Y coordinate
     */
    public Vector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    /**
     * Creates and returns a new 2D vector in which , it's coordinates are negative to current vector coordinates.
     * @return new 2D vector
     */
    public Vector2D negative(){
        return new Vector2D(-getX(),-getY());
    }

    /**
     * Creates and returns a new 2D vector in which , it's coordinates are division product of current vector coordinates and given {@code num}
     * @param num
     * @return new 2D vector
     */
    public Vector2D div(double num){
        return new Vector2D(getX()/num,getY()/num);
    }

    /**
     * Creates and returns a new 2D vector in which , it's coordinates are multiply product of current vector coordinates and given {@code num}
     * @param num
     * @return new 2D vector
     */
    public Vector2D mul(double num){
        return new Vector2D(getX()*num,getY()*num);
    }

    /**
     * Returns angle of current vector in standard math polar coordinates system.
     * @return angle in radians
     */
    public double getAngle() {
        return Math.atan2(getY(),getX());
    }

    /**
     * Calculates and returns size of current vector.
     * @return
     */
    public double size(){
        return Math.sqrt(Math.pow(getX(),2) + Math.pow(getY(),2));
    }


    /**
     * Checks if this vector is O = (0,0).
     * @return
     */
    public boolean isZeroVector(){
        return getX()==0 && getY()==0;
    }


    @Override
    public String toString() {
        return "(" + getX() + "," + getY() + ")";
    }

    //region Static methods

    /**
     * Static method that returns (0,0) vector.
     * @return
     */
    public static Vector2D zero(){
        return new Vector2D(0,0);
    }

    /**
     * Static method used to add two 2D vectors.
     * @param a
     * @param b
     * @return new 2D vector
     */
    public static Vector2D add(Vector2D a,Vector2D b){
        return new Vector2D(a.getX() + b.getX(),a.getY() + b.getY());
    }

    /**
     * Static method used to calculate distance between two 2D vectors.
     * @param a
     * @param b
     * @return
     */
    public static double distance(Vector2D a,Vector2D b){
        return Math.sqrt(Math.pow(a.getX() - b.getX(),2) + Math.pow(a.getY() - b.getY(),2));
    }

    /**
     * Static method used to create and return a Line2D object from two 2D vectors.
     * @param a
     * @param b
     * @return new Line2D object (The line that connects end points of two given vectors)
     */
    public static Line2D line(Vector2D a, Vector2D b){
        return new Line2D.Double(a.getX(),a.getY(),b.getX(),b.getY());
    }

    //endregion

}


