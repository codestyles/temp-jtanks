package ir.aut.GameEngine.Input;

import java.awt.event.*;
import java.util.ArrayList;

/**
 * <h1>Keyboard input handler</h1>
 * A singleton class that catches keyboard events.
 *
 * <h2>What should we do here ?</h2>
 * Nothing , this is an helper class.
 *
 * @author MMKH
 *
 */
public final class Keyboard {

    private static Keyboard instance = null;

    private ArrayList<Integer> pressedKeys;
    private Keyboard.KeyHandler keyHandler;

    /**
     * Gets the singleton instance of Keyboard.
     * @return
     */
    public static Keyboard getInstance(){
        if(instance==null)
            instance = new Keyboard();
        return instance;
    }

    private Keyboard() {
        pressedKeys = new ArrayList<>();
        keyHandler = new Keyboard.KeyHandler();
    }

    /**
     * Returns keyboard listener object
     * @return
     */
    public KeyListener getKeyListener() {
        return keyHandler;
    }

    /**
     * Checks if any key is pressed (or still pressed).
     * @return
     */
    public boolean isAnyKeyPressed(){
        return !pressedKeys.isEmpty();
    }

    /**
     * Checks if the given keyboard button is pressed or not (returns {@code true} until given keyboard button is released).<br>
     * Compare with {@code isKeyPressedOnce}
     * @param keyCode {@code KeyEvent.VK_*}
     * @return
     */
    public boolean isKeyPressed(int keyCode){
        return pressedKeys.contains(keyCode);
    }

    /**
     * Checks if the given keyboard button is pressed or not (returns {@code true} just once , and then {@code false} until given keyboard button is released and pressed again).<br>
     * Compare with {@code isKeyPressed}
     * @param keyCode {@code KeyEvent.VK_*}
     * @return
     */
    public boolean isKeyPressedOnce(int keyCode){
        return pressedKeys.remove(Integer.valueOf(keyCode));
    }

    /**
     * The keyboard handler internal class.
     *
     * @author MMKH
     *
     */
    class KeyHandler extends KeyAdapter {

        @Override
        public void keyPressed(KeyEvent e) {
            if(!pressedKeys.contains(e.getKeyCode()))
                pressedKeys.add(e.getKeyCode());
        }

        @Override
        public void keyReleased(KeyEvent e) {
            pressedKeys.remove(Integer.valueOf(e.getKeyCode()));
        }

    }

}
