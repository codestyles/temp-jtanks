package ir.aut.GameEngine.Input;

import ir.aut.GameEngine.Core.Camera;
import ir.aut.GameEngine.Vector.Vector2D;

import java.awt.event.*;

/**
 * <h1>Mouse input handler</h1>
 * A singleton class that catches mouse events.
 *
 * <h2>What should we do here ?</h2>
 * Nothing , this is an helper class.
 *
 * @author MMKH
 *
 */
public final class Mouse {

    private static Mouse instance = null;

    private boolean mousePressed;
    private int mouseButton;
    private int mouseX, mouseY;
    private Mouse.MouseHandler mouseHandler;


    /**
     * Gets the singleton instance of Mouse.
     * @return
     */
    public static Mouse getInstance(){
        if(instance==null)
            instance = new Mouse();
        return instance;
    }

    private Mouse() {
        mouseHandler = new Mouse.MouseHandler();
    }

    /**
     * Returns mouse listener object
     * @return
     */
    public MouseListener getMouseListener() {
        return mouseHandler;
    }

    /**
     * Checks if the given mouse button is pressed or not (returns {@code true} until given mouse button is released).<br>
     * Compare with {@code isMousePressedOnce}
     * @param mouseButton {@code MouseEvent.BUTTON_#}
     * @return
     */
    public boolean isMousePressed(int mouseButton) {
        return mousePressed && this.mouseButton==mouseButton;
    }

    /**
     * Checks if the given mouse button is pressed or not (returns {@code true} just once , and then {@code false} until given mouse button is released and pressed again).<br>
     * Compare with {@code isMousePressed}
     * @param mouseButton {@code MouseEvent.BUTTON_#}
     * @return
     */
    public boolean isMousePressedOnce(int mouseButton) {
        if(mousePressed && this.mouseButton==mouseButton){
            mousePressed = false;
            return true;
        }
        return false;
    }

    public int getMouseX() {
        return mouseX + (int)Camera.getInstance().getTransform().getLocation().getX();
    }

    public int getMouseY() {
        return mouseY + (int)Camera.getInstance().getTransform().getLocation().getY();
    }

    /**
     * Creates and returns a 2D vector with current mouse coordinates.
     * @return new 2D vector
     */
    public Vector2D getMouseCoordinates(){
        return new Vector2D(getMouseX(),getMouseY());
    }

    /**
     * The Mouse handler internal class.
     *
     * @author MMKH
     *
     */
    class MouseHandler extends MouseAdapter implements MouseMotionListener {

        @Override
        public void mousePressed(MouseEvent e) {
            mouseX = e.getX();
            mouseY = e.getY();
            mousePressed = true;
            mouseButton = e.getButton();
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            mousePressed = false;
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            mouseX = e.getX();
            mouseY = e.getY();
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            mouseX = e.getX();
            mouseY = e.getY();
        }
    }

}
