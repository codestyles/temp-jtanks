/*** In The Name of Allah ***/
package ir.aut;

import ir.aut.GameEngine.Input.Keyboard;
import ir.aut.GameEngine.Input.Mouse;
import ir.aut.GameEngine.Scene;

import java.awt.event.MouseMotionListener;

/**
 * Main game loop.
 * 
 * @author MMKH
 */
public class GameLoop implements Runnable {
	
	/**
	 * Frame Per Second.
	 * Higher is better, but any value above 24 is fine.
	 */
	public static final int FPS = 1000;
	
	private GameFrame canvas;

	public GameLoop(GameFrame frame) {
		canvas = frame;
	}
	
	/**
	 * This must be called before the game loop starts.
	 */
	public void init() {
		canvas.addKeyListener(Keyboard.getInstance().getKeyListener());
		canvas.addMouseListener(Mouse.getInstance().getMouseListener());
		canvas.addMouseMotionListener((MouseMotionListener) Mouse.getInstance().getMouseListener());
	}

	@Override
	public void run() {
	    synchronized (Scene.getInstance()) {
            Scene.getInstance().start();
        }
		boolean gameOver = false;
		while (!gameOver) {
			try {
				long start = System.currentTimeMillis();
				//
				Scene.getInstance().update();
				canvas.render();
				gameOver = Scene.getInstance().isGameOver();
				//
				long delay = (1000 / FPS) - (System.currentTimeMillis() - start) ;
				if (delay > 0)
					Thread.sleep(delay);
			} catch (InterruptedException ex) {
			}
		}
		canvas.render();
	}
}
