package ir.aut.TanksGame;

import ir.aut.GameEngine.Components.Animation.LocationAnimator;
import ir.aut.GameEngine.Components.Collider.BoxCollider;
import ir.aut.GameEngine.Components.Rendering.SpriteRenderer;
import ir.aut.GameEngine.Components.Transform;
import ir.aut.GameEngine.Core.GameObject;
import ir.aut.GameEngine.Vector.Vector2D;
import ir.aut.TanksGame.Components.Damageable;
import ir.aut.TanksGame.Components.SoftWallScript;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class SoftWall extends GameObject {

    private static BufferedImage sprite;

    /**
     * Creates a new GameObject with given id and transform.
     *
     * @param id
     * @param transform
     */
    public SoftWall(String id, Transform transform) {
        super(id, transform);
        renderer = new SpriteRenderer();
        try {
            if(SoftWall.sprite==null)
                SoftWall.sprite = ImageIO.read(getClass().getResource("/ir/aut/TanksGame/Resources/Images/softWall.png"));

        } catch (IOException e) {
            e.printStackTrace();
        }
        renderer.setStatic(true);
        renderer.setSprite(SoftWall.sprite);
        collider = new BoxCollider();
//        animator = new LocationAnimator(new Vector2D(500,500),1,true);

        components.add(new Damageable());
        components.add(new SoftWallScript());
    }
}
