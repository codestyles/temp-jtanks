package ir.aut.TanksGame;

import ir.aut.GameEngine.Components.Collider.BoxCollider;
import ir.aut.GameEngine.Components.Rendering.SpriteRenderer;
import ir.aut.GameEngine.Components.Transform;
import ir.aut.GameEngine.Core.GameObject;
import ir.aut.GameEngine.Scene;
import ir.aut.GameEngine.Vector.Vector2D;
import ir.aut.TanksGame.Components.PlayerTankGun;
import ir.aut.TanksGame.Components.PlayerTankMovement;
import ir.aut.TanksGame.Components.MissleBulletScript;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class Tank extends GameObject {

    private static BufferedImage sprite;


    public Tank(String id,Transform transform) {
        super(id,transform);
        this.renderer = new SpriteRenderer();
        try {
            if(Tank.sprite==null)
                Tank.sprite = ImageIO.read(getClass().getResource("/ir/aut/TanksGame/Resources/Images/tank.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        renderer.setSprite(Tank.sprite);
        BoxCollider collider = new BoxCollider();
        this.collider = collider;
//        this.animator = new Animator("/ir/aut/TanksGame/Resources/TestAnim",4,500,true);
        if(id.equals("Player")) {
            PlayerTankMovement playerTankMovement = new PlayerTankMovement();
            components.add(playerTankMovement);

         //   this.soundPlayer = new SoundPlayer("/ir/aut/TanksGame/Resources/Audio/MineBoom.wav",true,SoundPlayer.DYNAMIC_VOLUME);

        }
        if(id.equals("TankGun")) {
//            components.add(new PlayerTankGun());
        }
        if(id.equals("Player2")){
//            components.add(new MissleBulletScript());
         //   this.soundPlayer = new SoundPlayer("/ir/aut/TanksGame/Resources/Audio/MineBoom.wav",false,SoundPlayer.DYNAMIC_VOLUME);
        }

    }
}
