package ir.aut.TanksGame;

import ir.aut.GameEngine.Components.Animation.LocationAnimator;
import ir.aut.GameEngine.Components.Rendering.SpriteRenderer;
import ir.aut.GameEngine.Components.Transform;
import ir.aut.GameEngine.Core.GameObject;
import ir.aut.GameEngine.Scene;
import ir.aut.GameEngine.Vector.Vector2D;
import ir.aut.TanksGame.Components.PlayerTankGun;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class TankGunMissle extends GameObject {

    private static BufferedImage sprite;

    /**
     * Creates a new GameObject with given id and transform.
     *
     * @param id
     * @param transform
     */
    public TankGunMissle(String id, Transform transform) {
        super(id, transform);
        this.renderer = new SpriteRenderer();
        try {
            if(TankGunMissle.sprite==null)
                TankGunMissle.sprite = ImageIO.read(getClass().getResource("/ir/aut/TanksGame/Resources/Images/tankGun01.png"));

        } catch (IOException e) {
            e.printStackTrace();
        }
        renderer.setSprite(TankGunMissle.sprite);
        animator = new LocationAnimator(Vector2D.zero(),3,0.15,false);

        components.add(new PlayerTankGun());




    }
}
