package ir.aut.TanksGame;

import ir.aut.GameEngine.Components.Animation.RotationAnimator;
import ir.aut.GameEngine.Components.Collider.BoxCollider;
import ir.aut.GameEngine.Components.Rendering.SpriteRenderer;
import ir.aut.GameEngine.Components.Transform;
import ir.aut.GameEngine.Core.GameObject;
import ir.aut.TanksGame.Components.MissleBulletScript;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class MissleBullet extends GameObject {

    private static BufferedImage sprite;

    /**
     * Creates a new GameObject with given id and transform.
     *
     * @param id
     * @param transform
     */
    public MissleBullet(String id, Transform transform) {
        super(id, transform);
        this.renderer = new SpriteRenderer();
        try {
            if(MissleBullet.sprite==null)
                MissleBullet.sprite = ImageIO.read(getClass().getResource("/ir/aut/TanksGame/Resources/Images/HeavyBullet.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        renderer.setSprite(MissleBullet.sprite);
        this.collider = new BoxCollider();
        collider.setTrigger(true);
        animator = new RotationAnimator(0,400,true);
        components.add(new MissleBulletScript());
    }
}
