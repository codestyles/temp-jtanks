package ir.aut.TanksGame.Components;

import ir.aut.GameEngine.Components.Component;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class SoftWallScript extends Component {
    private BufferedImage[] sprites;

    @Override
    public void start() {
        try {
            sprites = new BufferedImage[]{ImageIO.read(getClass().getResource("/ir/aut/TanksGame/Resources/Images/softWall3.png")) , ImageIO.read(getClass().getResource("/ir/aut/TanksGame/Resources/Images/softWall2.png")) , ImageIO.read(getClass().getResource("/ir/aut/TanksGame/Resources/Images/softWall1.png"))};
        } catch (IOException e) {
            e.printStackTrace();
        }
//        getGameObject().getAnimator().play(true,null);
    }

    @Override
    public void update() {
        int health = getGameObject().getComponents(Damageable.class).get(0).getHealth();
        if(health==0){
            getGameObject().destroy();
        } else if(health/25 < 3){
            getGameObject().getRenderer().setSprite(sprites[health/25]);
        }

    }

    @Override
    public void onGUI(Graphics2D g) {

    }
}
