package ir.aut.TanksGame.Components;

import ir.aut.GameEngine.Components.Component;

import java.awt.*;

public class Damageable extends Component {

    private int health;

    @Override
    public void start() {
        health = 100;
    }

    @Override
    public void update() {

    }

    @Override
    public void onGUI(Graphics2D g) {

    }


    public int getHealth() {
        return health;
    }

    public void applyDamage(int damage) {
        health = Math.max(health - damage , 0);
    }

    public void repair(){
        health = 100;
    }
}
