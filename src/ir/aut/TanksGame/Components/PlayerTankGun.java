package ir.aut.TanksGame.Components;

import ir.aut.GameEngine.Components.Animation.LocationAnimator;
import ir.aut.GameEngine.Components.Component;
import ir.aut.GameEngine.Components.Transform;
import ir.aut.GameEngine.Input.Mouse;
import ir.aut.GameEngine.Scene;
import ir.aut.GameEngine.Vector.Vector2D;
import ir.aut.TanksGame.MissleBullet;

import java.awt.*;
import java.awt.event.MouseEvent;

public class PlayerTankGun extends Component {

    @Override
    public void start() {

    }

    @Override
    public void update() {
            getGameObject().getTransform().rotate(Transform.lookAt(getGameObject().getTransform(), Mouse.getInstance().getMouseCoordinates()));

            ((LocationAnimator)(getGameObject().getAnimator())).setFirstLoc(Vector2D.add(getGameObject().getParent().getTransform().getLocation(),getGameObject().getTransform().getPivot().negative()));
            ((LocationAnimator)(getGameObject().getAnimator())).setLoc(Vector2D.add(getGameObject().getParent().getTransform().getLocation(),getGameObject().getTransform().forward().mul(-10)));
            if(Mouse.getInstance().isMousePressedOnce(MouseEvent.BUTTON1)){

                MissleBullet missleBullet = new MissleBullet("PlayerTankMissleBullet",new Transform(Vector2D.add(getGameObject().getTransform().getLocation(),getGameObject().getTransform().forward().mul(50)),getGameObject().getTransform().getRotation(),new Vector2D(23 * 1.5,9 * 1.5),Vector2D.zero()));
                Scene.getInstance().instantiate(missleBullet);
                getGameObject().getAnimator().play(true, () -> {
                    getGameObject().getTransform().setLocation(Vector2D.zero());
                    return false;
                });
            }
    }

    @Override
    public void onGUI(Graphics2D g) {
        if(Scene.debuggingMode) {
            g.setColor(Color.CYAN);
            //Vector2D.add(
//        Vector2D l = Vector2D.add(Vector2D.add(getGameObject().getParent().getTransform().getLocation(),getGameObject().getTransform().getPivot().negative()),getGameObject().getTransform().forward().mul(-7));
            Vector2D l = Vector2D.add(getGameObject().getParent().getTransform().getLocation(), getGameObject().getTransform().forward().mul(-10));//,getGameObject().getTransform().forward().mul(-7));
            g.fillRect((int) l.getX(), (int) l.getY(), 5, 5);
        }
    }
}
