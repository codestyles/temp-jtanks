package ir.aut.TanksGame.Components;

import ir.aut.GameEngine.Components.Collider.CollisionHandler;
import ir.aut.GameEngine.Components.Component;
import ir.aut.GameEngine.Components.Transform;
import ir.aut.GameEngine.Core.Camera;
import ir.aut.GameEngine.Core.GameObject;
import ir.aut.GameEngine.Input.Keyboard;
import ir.aut.GameEngine.Input.Mouse;
import ir.aut.GameEngine.Scene;
import ir.aut.GameEngine.Vector.Vector2D;
import ir.aut.TanksGame.TankGunMissle;

import java.awt.*;
import java.awt.event.KeyEvent;


public class PlayerTankMovement extends Component implements CollisionHandler {
    private boolean reverseMove ;
    @Override
    public void start() {
//        getGameObject().getAnimator().play(true,null);
//        getGameObject().getSoundPlayer().play(null);

        TankGunMissle tankGunMissle = new TankGunMissle("PlayerGunMissle",new Transform(Vector2D.zero(),0,new Vector2D(110,110),new Vector2D(-17,0)));
        getGameObject().addChildGameObject(tankGunMissle);
        Scene.getInstance().instantiate(tankGunMissle);
    }

    @Override
    public void update() {
//        if(Keyboard.getInstance().isKeyPressed(KeyEvent.VK_L))
//            Camera.getInstance().getTransform().move(new Vector2D(10,10));
        if(Keyboard.getInstance().isKeyPressed(KeyEvent.VK_UP) || Keyboard.getInstance().isKeyPressed(KeyEvent.VK_DOWN) || Keyboard.getInstance().isKeyPressed(KeyEvent.VK_LEFT) || Keyboard.getInstance().isKeyPressed(KeyEvent.VK_RIGHT)) {

//            Camera.getInstance().getTransform().move(new Vector2D((Keyboard.getInstance().isKeyPressed(KeyEvent.VK_RIGHT) ? 5 : 0) + (Keyboard.getInstance().isKeyPressed(KeyEvent.VK_LEFT) ? -5 : 0) ,(Keyboard.getInstance().isKeyPressed(KeyEvent.VK_DOWN) ? 5 : 0) + (Keyboard.getInstance().isKeyPressed(KeyEvent.VK_UP) ? -5 : 0) ));
            double angle =  Math.atan2(((Keyboard.getInstance().isKeyPressed(KeyEvent.VK_DOWN) ? -1 : 0) + (Keyboard.getInstance().isKeyPressed(KeyEvent.VK_UP) ? 1 : 0)),((Keyboard.getInstance().isKeyPressed(KeyEvent.VK_RIGHT) ? 1 : 0) + (Keyboard.getInstance().isKeyPressed(KeyEvent.VK_LEFT) ? -1 : 0)));
            //if(angle<0)
            //    angle+=Math.toRadians(360);
//            if(getGameObject().getTransform().getRotation() != angle) {
//                System.out.println("A : " + Math.toDegrees(angle));
//                System.out.println("C : " +  Math.toDegrees(getGameObject().getTransform().getRotation()));
                angle -= (getGameObject().getTransform().getRotation() > Math.toRadians(180) ? getGameObject().getTransform().getRotation() - Math.toRadians(360) : getGameObject().getTransform().getRotation());
//                boolean rotatePositive;
//                if (angle > Math.toRadians(180)) {
//                    if (getGameObject().getTransform().getRotation() > angle - Math.toRadians(180) && getGameObject().getTransform().getRotation() < angle) {
//                        // Rotate +
//                        rotatePositive = true;
//                    } else {
//                        // Rotate -
//                        rotatePositive = false;
//                    }
//                } else {
//                    if (getGameObject().getTransform().getRotation() > angle && getGameObject().getTransform().getRotation() < angle + Math.toRadians(180)) {
//                        // Rotate -
//                        rotatePositive = false;
//                    } else {
//                        // Rotate +
//                        rotatePositive = true;
//                    }
//                }
//                System.out.println("B : " + Math.toDegrees(angle));
                if(Math.toDegrees(angle) > 180)
                    angle -= Math.toRadians(360);
                if(Math.toDegrees(angle) < -180)
                    angle += Math.toRadians(360);

                if(Math.abs(Math.toDegrees(angle)) > 90) {
                    reverseMove = true;
                    angle = ((angle!=0 ? -Math.signum(angle) : 1)) * (Math.toRadians(180) - Math.abs(angle));
                } else {
                    reverseMove = false;
                }

//                angle =  Math.abs(angle - getGameObject().getTransform().getRotation());
//                System.out.println(Math.toDegrees((rotatePositive ? 1 : -1) * angle));
                if(Math.abs(Math.toDegrees(angle)) < 5) {
                    getGameObject().getTransform().rotate(angle);
                    getGameObject().getCollider().move(getGameObject().getTransform().forward().mul(Scene.deltaTime * 0.5 * (reverseMove ? -1 : 1)));
                }
                else {
                    getGameObject().getTransform().rotate(angle / 5);
                    getGameObject().getCollider().move(getGameObject().getTransform().forward().mul(Scene.deltaTime * 0.2 * (reverseMove ? -1 : 1)));
                }

//                getGameObject().getCollider().move(getGameObject().getTransform().forward().mul(Scene.deltaTime * 0.1));
//            }
//            else {
//            getGameObject().getCollider().move(new Vector2D((Keyboard.getInstance().isKeyPressed(KeyEvent.VK_RIGHT) ? 5 : 0) + (K1eyboard.getInstance().isKeyPressed(KeyEvent.VK_LEFT) ? -5 : 0), (Keyboard.getInstance().isKeyPressed(KeyEvent.VK_DOWN) ? 5 : 0) + (Keyboard.getInstance().isKeyPressed(KeyEvent.VK_UP) ? -5 : 0)));
//                getGameObject().getCollider().move(getGameObject().getTransform().forward().mul(Scene.deltaTime * 0.5));
//            }

        }


        // Camera Movement
        Vector2D cameraMover = Vector2D.add(Vector2D.add(getGameObject().getTransform().getLocation().mul(4),Mouse.getInstance().getMouseCoordinates()).div(5),new Vector2D(Scene.GAME_WIDTH/2,Scene.GAME_HEIGHT/2).negative());
        Camera.getInstance().getTransform().setLocation(new Vector2D(cameraMover.getX()*(5000-Scene.GAME_WIDTH)/5000,cameraMover.getY()*(13000-Scene.GAME_HEIGHT)/13000));

//    getGameObject().getCollider().rotate(Math.toRadians(0.5));
       // getGameObject().getCollider().rotate(Transform.lookAt(getGameObject().getTransform(), Mouse.getInstance().getMouseCoordinates()));
    }

    @Override
    public void onGUI(Graphics2D g) {

    }

    @Override
    public void OnTriggerHit(GameObject other) {
       // System.out.println(other.getId());
    }

    @Override
    public void OnColliderHit(GameObject other) {
        //System.out.println(other.getId());
    }
}
