package ir.aut.TanksGame.Components;

import ir.aut.GameEngine.Components.Animation.RotationAnimator;
import ir.aut.GameEngine.Components.Collider.CollisionHandler;
import ir.aut.GameEngine.Components.Component;
import ir.aut.GameEngine.Core.Camera;
import ir.aut.GameEngine.Core.GameObject;
import ir.aut.GameEngine.Scene;
import ir.aut.GameEngine.Vector.Vector2D;

import java.awt.*;
import java.util.Random;
import java.util.Timer;

public class MissleBulletScript extends Component implements CollisionHandler {

    private boolean hit;
    private long hitTime;
    private Vector2D randomDirection;
    @Override
    public void OnTriggerHit(GameObject other) {
        if(!other.getComponents(Damageable.class).isEmpty() && !hit) {
            getGameObject().getCollider().setActive(false);
            other.getComponents(Damageable.class).get(0).applyDamage(40);
            ((RotationAnimator)(getGameObject().getAnimator())).setSecondAngle(getGameObject().getTransform().getRotation() + Math.toRadians(359));
            getGameObject().getAnimator().play(true,null);
            Random r = new Random();
            randomDirection = new Vector2D(r.nextDouble() - 0.5,r.nextDouble() - 0.5).mul(Scene.deltaTime * 0.3);
            hit=true;
            hitTime = System.currentTimeMillis();
        }
    }

    @Override
    public void OnColliderHit(GameObject other) {

    }

    @Override
    public void start() {

    }

    @Override
    public void update() {
        if(!hit) {
            if (Vector2D.distance(getGameObject().getTransform().getLocation(), Camera.getInstance().getTransform().getLocation()) > Scene.GAME_WIDTH * 2) {
                System.out.println("Out of range , DESTROY .");
                getGameObject().destroy();
            } else
                getGameObject().getCollider().move(getGameObject().getTransform().forward().mul(Scene.deltaTime * 1));
        } else {
            if(System.currentTimeMillis() - hitTime > 1000) {
                getGameObject().destroy();
            } else {
                getGameObject().getTransform().move(randomDirection);
            }
        }

    }

    @Override
    public void onGUI(Graphics2D g) {

    }
}
