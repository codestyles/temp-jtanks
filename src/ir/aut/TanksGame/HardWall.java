package ir.aut.TanksGame;

import ir.aut.GameEngine.Components.Collider.BoxCollider;
import ir.aut.GameEngine.Components.Rendering.SpriteRenderer;
import ir.aut.GameEngine.Components.Transform;
import ir.aut.GameEngine.Core.GameObject;
import ir.aut.TanksGame.Components.Damageable;
import ir.aut.TanksGame.Components.SoftWallScript;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class HardWall extends GameObject {

    private static BufferedImage sprite;

    /**
     * Creates a new GameObject with given id and transform.
     *
     * @param id
     * @param transform
     */
    public HardWall(String id, Transform transform) {
        super(id, transform);
        renderer = new SpriteRenderer();
        try {
            if(HardWall.sprite==null)
                HardWall.sprite = ImageIO.read(getClass().getResource("/ir/aut/TanksGame/Resources/Images/hardWall.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        renderer.setStatic(true);
        renderer.setSprite(HardWall.sprite);

        collider = new BoxCollider();

    }
}