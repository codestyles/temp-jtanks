package ir.aut.TanksGame;

import ir.aut.GameEngine.Components.Collider.BoxCollider;
import ir.aut.GameEngine.Components.Rendering.SpriteRenderer;
import ir.aut.GameEngine.Components.Transform;
import ir.aut.GameEngine.Core.GameObject;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class Plant extends GameObject {

    private static BufferedImage sprite;

    /**
     * Creates a new GameObject with given id and transform.
     *
     * @param id
     * @param transform
     */
    public Plant(String id, Transform transform) {
        super(id, transform);
        renderer = new SpriteRenderer();
        try {
            if(Plant.sprite==null)
                Plant.sprite = ImageIO.read(getClass().getResource("/ir/aut/TanksGame/Resources/Images/tree.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        renderer.setStatic(true);
        renderer.setSprite(Plant.sprite);

    }
}